Maintenance module provides features to improve user experience on Drupal maintenance mode.

# Basic usage

Drupal allows you to set your site into maintenance mode in order to deploy code and database upgrades safely. Maintenance module provides a select list which listing all currently available themes, choose which theme the maintenance page should display in. If choose "System Default" the maintenance page will use the same theme as the rest of the site.

# Install

Download the archive or clone the repository.
Copy the maintenance/ folder in your modules folders (for example: sites/all/modules).
Go to the modules pages (/admin/build/modules), and enable the maintenance module.
Go to Site Maintenance page (/admin/settings/site-maintenance), choose which theme the maintenance page should display in and Save configuration.
From now on, the maintenance page has been changed as you want when you off-line your site.